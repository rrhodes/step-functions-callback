#!/usr/bin/env node
import 'source-map-support/register';
import { App } from '@aws-cdk/core';
import StepFunctionsCallbackStack from '../lib/step-functions-callback-stack';

const app = new App();
new StepFunctionsCallbackStack(app, 'StepFunctionsCallbackStack');
