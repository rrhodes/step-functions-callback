import { Construct, Duration, Stack, StackProps } from "@aws-cdk/core";
import { Code, Function as LambdaFunction, Runtime } from '@aws-cdk/aws-lambda';
import { SqsEventSource } from "@aws-cdk/aws-lambda-event-sources";
import { Queue } from '@aws-cdk/aws-sqs';
import { Context, ServiceIntegrationPattern, StateMachine, Task, TaskInput } from '@aws-cdk/aws-stepfunctions';
import { SendToQueue } from '@aws-cdk/aws-stepfunctions-tasks';
import { v4 as uuid } from 'uuid';
import { PolicyStatement } from "@aws-cdk/aws-iam";

export default class StepFunctionsCallbackStack extends Stack {
  public constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props);

    const queue = new Queue(this, 'Queue', {
      queueName: "callback-queue",
      retentionPeriod: Duration.minutes(1),
    });

    const callbackTask = new Task(this, 'CallbackTask', {
      task: new SendToQueue(queue, {
        messageBody: TaskInput.fromObject({
          "id": uuid(),
          "taskToken": Context.taskToken,
        }),
        integrationPattern: ServiceIntegrationPattern.WAIT_FOR_TASK_TOKEN,
      }),
    });

    const stateMachine = new StateMachine(this, 'StateMachine', {
      timeout: Duration.minutes(2),
      definition: {
        startState: callbackTask,
        id: 'callback-state-machine',
        endStates: [callbackTask],
      },
    });

    const callbackLambda = new LambdaFunction(this, 'Function', {
      code: Code.asset('./lambda'),
      description: 'SQS consumer for a Step Functions Callback task.',
      functionName: 'callback-function',
      handler: 'app.lambda_handler',
      runtime: Runtime.PYTHON_3_7,
    });

    const sendTaskPolicyStatement = new PolicyStatement({
      resources: [stateMachine.stateMachineArn],
      actions: ["states:SendTaskFailure", "states:SendTaskHeartbeat", "states:SendTaskSuccess"],
    });

    callbackLambda.addEventSource(new SqsEventSource(queue));
    callbackLambda.addToRolePolicy(sendTaskPolicyStatement);
  }
}
